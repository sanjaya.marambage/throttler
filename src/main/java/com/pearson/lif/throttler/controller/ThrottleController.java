package com.pearson.lif.throttler.controller;


import com.pearson.lif.throttler.integration.serviceactivator.MessagePublisher;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.bus.event.RefreshRemoteApplicationEvent;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.integration.support.MessageBuilder;
import org.springframework.integration.util.DynamicPeriodicTrigger;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.time.Duration;


/**
 * @author sanjaya on 2020-10-02
 */
@RestController
public class ThrottleController {


    @Autowired
    private MessagePublisher messageQueuePublisher;

    @Autowired
    ApplicationContext applicationContext;

//    @Autowired
//    private ThrottlingConfigsRepository throttlingConfigsRepository;
//
//    @Autowired
//    private CourseReadController courseReadController;

//    @Value("${throttle.admin.accesstoken}")
//    private String accessToken;

    private static final Logger LOGGER = LoggerFactory.getLogger(ThrottleController.class);

//    @GetMapping("/test")
//    public String test() {
//        courseReadController.sendTestXLBulk();
//        ThrottlingConfigs throttlingConfigs = new ThrottlingConfigs();
//        throttlingConfigs.setId("COURSE_XL");
//        throttlingConfigs.setDelay(5000);
//        throttlingConfigsRepository.save(throttlingConfigs);
//        return "tested";
//    }

    /**
     * This starts or stop consumption of messages for a given channel
     *
     * @param channel
     * @param action
     * @return
     */
    @GetMapping("/inbound/{action}")
    public ResponseEntity<String> invokeSendMessage(
            //@RequestHeader(
            //value = "x-token", required = true) String token
            @RequestHeader(value = "channel", required = true) String channel,
            @PathVariable(value = "action", required = true) String action) {

//        if (!token.equals(accessToken)) {
//            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
//        }
        if (!action.equals("start") && !action.equals("stop")) {
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }

        if (!channel.contains("_")) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        String[] channelType = channel.split("_");

        //ThrottlingConfigs throttlingConfigs = null;

        String controlCommand = "@";
        if ("course".equals(channelType[0])) {

            if ("in".equals(channelType[1])) {
                // throttlingConfigs = throttlingConfigsRepository.findById(COURSE_IN).get();
                controlCommand = controlCommand.concat("courseIn");
//            } else if ("xl".equals(channelType[1])) {
//                throttlingConfigs = throttlingConfigsRepository.findById(COURSE_XL).get();
//                controlCommand = controlCommand.concat("writeControllerXlInbound");
//            } else if ("master".equals(channelType[1])) {applicationContext
//                throttlingConfigs = throttlingConfigsRepository.findById(COURSE_MASTERING).get();
//                controlCommand = controlCommand.concat("writeControllerMasterInbound");
//            } else if ("peg".equals(channelType[1])) {
//                throttlingConfigs = throttlingConfigsRepository.findById(COURSE_PEGASUS).get();
//                controlCommand = controlCommand.concat("writeControllerPegasusInbound");
//            } else if ("other".equals(channelType[1])) {
//                throttlingConfigs = throttlingConfigsRepository.findById(COURSE_OTHER).get();
//                controlCommand = controlCommand.concat("writeControllerOtherInbound");
//            } else {
//                return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
//            }
//        } else if ("enroll".equals(channelType[0])) {
//
//            if ("in".equals(channelType[1])) {
//                throttlingConfigs = throttlingConfigsRepository.findById(ENROLL_IN).get();
//                controlCommand = controlCommand.concat("jmsEnrollIn");
//            } else if ("xl".equals(channelType[1])) {
//                throttlingConfigs = throttlingConfigsRepository.findById(ENROLL_XL).get();
//                controlCommand = controlCommand.concat("enrollXlInbound");
//            } else if ("master".equals(channelType[1])) {
//                throttlingConfigs = throttlingConfigsRepository.findById(ENROLL_MASTERING).get();
//                controlCommand = controlCommand.concat("enrollMasterInbound");
//            } else if ("peg".equals(channelType[1])) {
//                throttlingConfigs = throttlingConfigsRepository.findById(ENROLL_PEGASUS).get();
//                controlCommand = controlCommand.concat("enrollPegasusInbound");
//            } else if ("other".equals(channelType[1])) {
//                throttlingConfigs = throttlingConfigsRepository.findById(ENROLL_OTHER).get();
//                controlCommand = controlCommand.concat("enrollOtherInbound");
//            } else {
//                return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
//            }
//        } else {
//            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
            }
            controlCommand = controlCommand.concat("." + action + "()");
//
//        if (action.equals("start")) {
//            throttlingConfigs.setIsControllerThrottled("on");
//        } else {
//            throttlingConfigs.setIsControllerThrottled("off");
        }

        //throttlingConfigsRepository.save(throttlingConfigs);

        messageQueuePublisher.sendPayload(MessageBuilder.withPayload(controlCommand).build(), "controlChannel");

        System.out.println(channel + " adapter command" + controlCommand + " " + action + "ed");

        return new ResponseEntity<>(channel + " channel " + action + " done!", HttpStatus.OK);

    }



    @RequestMapping(value = "/inbound/rate", method = RequestMethod.POST)
    public ResponseEntity<String> changeRate(@RequestHeader(value = "rate", required = true) long rate) {


        //List<ThrottlingConfigs> throttlingConfigsList = throttlingConfigsRepository.findAll();
        //throttlingConfigsList.forEach((config) -> {
            String channel = "periodicTrigger";

          //  if (!COURSE_IN.equalsIgnoreCase(config.getId()) && !ENROLL_IN.equalsIgnoreCase(config.getId())) {
        //        channel = channel.concat(CaseFormat.UPPER_UNDERSCORE.to(CaseFormat.UPPER_CAMEL, config.getId()));

                DynamicPeriodicTrigger trigger = (DynamicPeriodicTrigger) applicationContext.getBean("periodicTriggerCourse");
                trigger.setDuration(Duration.ofMillis(rate));
                LOGGER.debug("rate set for " + channel);
           // }

      //  });

        return new ResponseEntity<String>(String.valueOf(rate), HttpStatus.CREATED);

   }

    @RequestMapping(value = "/config/change", method = RequestMethod.POST)
    public ResponseEntity<String> changeConfig(@RequestBody String body) {

        LOGGER.info(body);

        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders httpHeaders = new HttpHeaders();
        // Set content_type to json or an error of 415 will be reported
        httpHeaders.add(HttpHeaders.CONTENT_TYPE,"application/json");
        HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<>(null,httpHeaders);
        // Use the post method to access the real refresh link
        ResponseEntity<String> stringResponseEntity = restTemplate.postForEntity("http://localhost:8080/actuator/bus-refresh",
                request, String.class);

        return new ResponseEntity<String>(String.valueOf(body), HttpStatus.CREATED);

    }



}
