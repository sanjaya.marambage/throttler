package com.pearson.lif.throttler.config;


import com.pearson.lif.throttler.integration.serviceactivator.MessagePublisher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.support.channel.BeanFactoryChannelResolver;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.core.DestinationResolver;

@Configuration
public class MessageQueueConfig {

    @Autowired
    private ApplicationContext context;

    @Bean
    public DestinationResolver channelResolver()  {

        DestinationResolver<MessageChannel> channelResolver = new BeanFactoryChannelResolver(context);
        return channelResolver;
    }

    @Bean
    public MessagePublisher textMessageQueuePublisher()  {

        MessagePublisher messageQueuePublisher = new MessagePublisher();
        messageQueuePublisher.setChannelResolver(channelResolver());
        return messageQueuePublisher;
    }

}
