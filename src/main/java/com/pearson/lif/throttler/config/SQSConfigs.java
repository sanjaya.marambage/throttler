package com.pearson.lif.throttler.config;


import com.amazon.sqs.javamessaging.ProviderConfiguration;
import com.amazon.sqs.javamessaging.SQSConnectionFactory;
import com.amazon.sqs.javamessaging.SQSSession;
import com.amazonaws.services.sqs.AmazonSQSClientBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;
import org.springframework.integration.util.DynamicPeriodicTrigger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;

import javax.jms.Queue;
import javax.jms.QueueSession;

import static javax.jms.Session.AUTO_ACKNOWLEDGE;
import static javax.jms.Session.DUPS_OK_ACKNOWLEDGE;

/**
 * @author sanjaya on 2021-01-20
 */
@RefreshScope
@Configuration
@ImportResource(value = {"classpath:spring-integration.xml"})
public class SQSConfigs {

    private static final Logger LOGGER = LoggerFactory.getLogger(SQSConfigs.class);


    @Value("${rate}")
    private int rate;

    @Value("${queue}")
    private String queueName;

    @Bean
    public SQSConnectionFactory sqsConnectionFactory() {


        SQSConnectionFactory connectionFactory = new SQSConnectionFactory(
                new ProviderConfiguration().withNumberOfMessagesToPrefetch(0),
                AmazonSQSClientBuilder.defaultClient()
        );
        return connectionFactory;
    }

    @Bean
    public Queue courseInBoundQueue() {
        Queue queue = null;
        try {
            queue = sqsConnectionFactory().createQueueConnection().createQueueSession(false, AUTO_ACKNOWLEDGE)
                    .createQueue(queueName);
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
        }
        return queue;
    }



    @Bean
    public QueueSession courseOutBoundSession() {
        QueueSession queue = null;
        try {
            queue = sqsConnectionFactory().createQueueConnection().createQueueSession(false, AUTO_ACKNOWLEDGE);

        } catch (Exception e) {
            LOGGER.error(e.getMessage());
        }
        return queue;
    }

    @Bean
    DynamicPeriodicTrigger periodicTriggerCourse(){

        DynamicPeriodicTrigger trigger = new DynamicPeriodicTrigger(rate);
        LOGGER.info("========rate ===============");
        LOGGER.info("======"+rate);
        trigger.setFixedRate(true);
        return trigger;
    }

}
