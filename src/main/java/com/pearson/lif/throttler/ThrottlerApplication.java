package com.pearson.lif.throttler;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = {"com.pearson.lif.throttler"})
public class ThrottlerApplication {

	public static void main(String[] args) {
		SpringApplication.run(ThrottlerApplication.class, args);
	}

}
