package com.pearson.lif.throttler.integration.serviceactivator;

import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.core.DestinationResolver;
import org.springframework.stereotype.Component;

/**
 * @author sanjaya on 2021-01-21
 */

public class MessagePublisher {

    private DestinationResolver channelResolver;

    public void setChannelResolver(DestinationResolver channelResolver) {
        this.channelResolver = channelResolver;
    }

    public <T> void sendPayload(Message<T> payload, String channel) {

        if (channelResolver != null) {
            System.out.println("Got in");
            MessageChannel messageChannel = (MessageChannel) channelResolver.resolveDestination(channel);
            messageChannel.send(payload, 30);

        }
    }

}
