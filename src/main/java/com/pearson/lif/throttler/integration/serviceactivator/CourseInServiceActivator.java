package com.pearson.lif.throttler.integration.serviceactivator;

import com.amazonaws.services.sqs.model.Message;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.integration.support.MessageBuilder;
import org.springframework.messaging.handler.annotation.Headers;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Service;

import javax.jms.*;
import java.util.Map;

/**
 * @author sanjaya on 2021-01-21
 */
@Service
public class CourseInServiceActivator {

    private static final Logger LOGGER = LoggerFactory.getLogger(CourseInServiceActivator.class);

    @Autowired
    MessagePublisher messagePublisher;


    @Autowired
    @Qualifier("courseOutBoundSession")
    QueueSession courseOutBoundSession;


    @ServiceActivator
    public void consumeMessage(String payload) throws JMSException {

        System.out.println(payload);

//        Queue courseOutBoundQueue = courseOutBoundSession.createQueue("lif_course_out.fifo");
//        //LOGGER.debug("Portal Course payload for write Controller: {}",payload);
//
//        MessageProducer producer = courseOutBoundSession.createProducer(courseOutBoundQueue);
//
//        TextMessage message = courseOutBoundSession.createTextMessage(payload);
//        //messagePublisher.sendPayload(MessageBuilder.withPayload(payload).build(), "courseOutBoundChannel");
//// Set the message group ID
//        message.setStringProperty("JMSXGroupID", "Default");
//
//// You can also set a custom message deduplication ID
//// message.setStringProperty("JMS_SQS_DeduplicationId", "hello");
//// Here, it's not needed because content-based deduplication is enabled for the queue
//
//// Send the message
//        producer.send(message);


    }

}
