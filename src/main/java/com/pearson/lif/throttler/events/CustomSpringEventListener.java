package com.pearson.lif.throttler.events;

import com.pearson.lif.throttler.controller.ThrottleController;
import com.pearson.lif.throttler.integration.serviceactivator.MessagePublisher;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.bus.event.RefreshRemoteApplicationEvent;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.EventListener;
import org.springframework.integration.support.MessageBuilder;
import org.springframework.integration.util.DynamicPeriodicTrigger;
import org.springframework.stereotype.Component;

import java.time.Duration;

/**
 * @author sanjaya on 2021-03-06
 */
@RefreshScope
@Component
public class CustomSpringEventListener implements ApplicationListener<RefreshRemoteApplicationEvent> {

    @Value("${rate}")
    private int rate;

    @Value("${isthrottled}")
    private boolean isThrottled;

    private static final Logger LOGGER = LoggerFactory.getLogger(CustomSpringEventListener.class);

    @Autowired
    ApplicationContext applicationContext;

    @Autowired
    private MessagePublisher messageQueuePublisher;

    @Override
    @EventListener
    public void onApplicationEvent(RefreshRemoteApplicationEvent event) {
        LOGGER.info("Received spring custom event - "+ event.toString());
        LOGGER.info("################## rate- "+ rate);
        //List<ThrottlingConfigs> throttlingConfigsList = throttlingConfigsRepository.findAll();
        //throttlingConfigsList.forEach((config) -> {
        String channel = "periodicTrigger";

        //  if (!COURSE_IN.equalsIgnoreCase(config.getId()) && !ENROLL_IN.equalsIgnoreCase(config.getId())) {
        //        channel = channel.concat(CaseFormat.UPPER_UNDERSCORE.to(CaseFormat.UPPER_CAMEL, config.getId()));

        DynamicPeriodicTrigger trigger = (DynamicPeriodicTrigger) applicationContext.getBean("periodicTriggerCourse");
        trigger.setDuration(Duration.ofMillis(rate));
        LOGGER.info("rate set for " + channel);

        //Throttle check
        String controlCommand = "@courseIn";

        String action = isThrottled?"stop":"start";

        controlCommand = controlCommand.concat("." + action + "()");

        messageQueuePublisher.sendPayload(MessageBuilder.withPayload(controlCommand).build(), "controlChannel");

        LOGGER.info(channel + " adapter command" + controlCommand + " " + action + "ed");
    }

}
